﻿using System.Diagnostics;
using Acrotech.PortableLogAdapter.Loggers;

namespace Acrotech.PortableLogAdapter.Managers
{
    /// <summary>
    /// Debug Log Manager always returns a <see cref="Acrotech.PortableLogAdapter.Loggers.DelegateLogger"/> that writes to the System.Diagnostics.Debug output
    /// </summary>
    public class DebugLogManager : ILogManager
    {
        /// <summary>
        /// Debug Logger Name
        /// </summary>
        public const string LoggerName = "Debug Logger";

        /// <summary>
        /// Default Debug Log Manager Instance
        /// </summary>
        public static readonly DebugLogManager Default = new DebugLogManager();

        private DebugLogManager()
        {
        }

        /// <summary>
        /// Default Debug Logger Instace
        /// </summary>
#if DEBUG
        public static readonly ILogger Logger = CreateDebugLogger(x => Debug.WriteLine(x));
#else
        public static ILogger Logger { get; private set; }

        /// <summary>
        /// Initializes the Debug Logger with the provided log action
        /// </summary>
        /// <param name="logAction">Action to call when logging events occur</param>
        /// <remarks>This function is only available in release mode</remarks>
        public static DebugLogManager Initialize(System.Action<string> logAction)
        {
            Logger = CreateDebugLogger(logAction);

            return Default;
        }
#endif

        private static ILogger CreateDebugLogger(System.Action<string> logAction)
        {
            return new DelegateLogger(LoggerName, LogLevel.Trace, logAction);
        }

        /// <inheritdoc/>
        /// <remarks>Always returns the default debug logger instance</remarks>
        public ILogger GetLogger(string name)
        {
#if !DEBUG
            if (Logger == null)
            {
                throw new System.InvalidOperationException("Initialize must be called before logging begins");
            }
#endif

            return Logger;
        }
    }
}
