﻿using Acrotech.PortableLogAdapter.Loggers;

namespace Acrotech.PortableLogAdapter.Managers
{
    /// <summary>
    /// Null Log Manager always returns the default Null Logger.
    /// </summary>
    public class NullLogManager : ILogManager
    {
        /// <summary>
        /// Default Null Log Manager Instance
        /// </summary>
        public static readonly NullLogManager Default = new NullLogManager();

        private NullLogManager()
        {
        }

        /// <inheritdoc/>
        /// <remarks>Always returns the default debug logger instance</remarks>
        public ILogger GetLogger(string name)
        {
            return NullLogger.Default;
        }
    }
}
