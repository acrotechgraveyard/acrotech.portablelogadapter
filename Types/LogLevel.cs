﻿namespace Acrotech.PortableLogAdapter
{
    /// <summary>
    /// Log Levels
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// For very detailed log messages
        /// </summary>
        Trace = 0,
        /// <summary>
        /// For diagnostic log messages
        /// </summary>
        Debug = 1,
        /// <summary>
        /// For informational log messages
        /// </summary>
        Info = 2,
        /// <summary>
        /// For warning log messages
        /// </summary>
        Warn = 3,
        /// <summary>
        /// For error log messages
        /// </summary>
        Error = 4,
        /// <summary>
        /// For critical failure log messages
        /// </summary>
        Fatal = 5,
        /// <summary>
        /// Disable logging
        /// </summary>
        Off = 6,
    }
}
