﻿using System;

namespace Acrotech.PortableLogAdapter
{
    /// <summary>
    /// Extensions to simplify logging calls by replacing the level param with an explicit function call
    /// </summary>
    public static partial class ExtensionMethods
    {
        #region Trace (0)

        /// <summary>
        /// Log a Trace message by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        public static void Trace(this ILogger logger, string format, params object[] args)
        {
            if (logger != null)
            {
                logger.Log(LogLevel.Trace, format, args);
            }
        }

        /// <summary>
        /// Log a Trace message by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        public static void Trace(this ILogger logger, Func<string> messageCreator)
        {
            if (logger != null)
            {
                logger.Log(LogLevel.Trace, messageCreator);
            }
        }

        /// <summary>
        /// Log a Trace <see cref="System.Exception"/> by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="exception">Exception</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        public static void TraceException(this ILogger logger, Exception exception, string format, params object[] args)
        {
            if (logger != null)
            {
                logger.LogException(LogLevel.Trace, exception, format, args);
            }
        }

        /// <summary>
        /// Log a Trace <see cref="System.Exception"/> by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="exception">Exception</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        public static void TraceException(this ILogger logger, Exception exception, Func<string> messageCreator)
        {
            if (logger != null)
            {
                logger.LogException(LogLevel.Trace, exception, messageCreator);
            }
        }

        /// <summary>
        /// Check if Trace is enabled for this logger
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <returns>True if the level is enabled, false otherwise</returns>
        public static bool IsTraceEnabled(this ILogger logger)
        {
            return logger != null && logger.IsEnabled(LogLevel.Trace);
        }

        #endregion

        #region Debug (1)

        /// <summary>
        /// Log a Debug message by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        public static void Debug(this ILogger logger, string format, params object[] args)
        {
            if (logger != null)
            {
                logger.Log(LogLevel.Debug, format, args);
            }
        }

        /// <summary>
        /// Log a Debug message by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        public static void Debug(this ILogger logger, Func<string> messageCreator)
        {
            if (logger != null)
            {
                logger.Log(LogLevel.Debug, messageCreator);
            }
        }

        /// <summary>
        /// Log a Debug <see cref="System.Exception"/> by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="exception">Exception</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        public static void DebugException(this ILogger logger, Exception exception, string format, params object[] args)
        {
            if (logger != null)
            {
                logger.LogException(LogLevel.Debug, exception, format, args);
            }
        }

        /// <summary>
        /// Log a Debug <see cref="System.Exception"/> by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="exception">Exception</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        public static void DebugException(this ILogger logger, Exception exception, Func<string> messageCreator)
        {
            if (logger != null)
            {
                logger.LogException(LogLevel.Debug, exception, messageCreator);
            }
        }

        /// <summary>
        /// Check if Debug is enabled for this logger
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <returns>True if the level is enabled, false otherwise</returns>
        public static bool IsDebugEnabled(this ILogger logger)
        {
            return logger != null && logger.IsEnabled(LogLevel.Debug);
        }

        #endregion

        #region Info (2)

        /// <summary>
        /// Log a Info message by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        public static void Info(this ILogger logger, string format, params object[] args)
        {
            if (logger != null)
            {
                logger.Log(LogLevel.Info, format, args);
            }
        }

        /// <summary>
        /// Log a Info message by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        public static void Info(this ILogger logger, Func<string> messageCreator)
        {
            if (logger != null)
            {
                logger.Log(LogLevel.Info, messageCreator);
            }
        }

        /// <summary>
        /// Log a Info <see cref="System.Exception"/> by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="exception">Exception</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        public static void InfoException(this ILogger logger, Exception exception, string format, params object[] args)
        {
            if (logger != null)
            {
                logger.LogException(LogLevel.Info, exception, format, args);
            }
        }

        /// <summary>
        /// Log a Info <see cref="System.Exception"/> by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="exception">Exception</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        public static void InfoException(this ILogger logger, Exception exception, Func<string> messageCreator)
        {
            if (logger != null)
            {
                logger.LogException(LogLevel.Info, exception, messageCreator);
            }
        }

        /// <summary>
        /// Check if Info is enabled for this logger
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <returns>True if the level is enabled, false otherwise</returns>
        public static bool IsInfoEnabled(this ILogger logger)
        {
            return logger != null && logger.IsEnabled(LogLevel.Info);
        }

        #endregion

        #region Warn (3)

        /// <summary>
        /// Log a Warn message by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        public static void Warn(this ILogger logger, string format, params object[] args)
        {
            if (logger != null)
            {
                logger.Log(LogLevel.Warn, format, args);
            }
        }

        /// <summary>
        /// Log a Warn message by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        public static void Warn(this ILogger logger, Func<string> messageCreator)
        {
            if (logger != null)
            {
                logger.Log(LogLevel.Warn, messageCreator);
            }
        }

        /// <summary>
        /// Log a Warn <see cref="System.Exception"/> by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="exception">Exception</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        public static void WarnException(this ILogger logger, Exception exception, string format, params object[] args)
        {
            if (logger != null)
            {
                logger.LogException(LogLevel.Warn, exception, format, args);
            }
        }

        /// <summary>
        /// Log a Warn <see cref="System.Exception"/> by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="exception">Exception</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        public static void WarnException(this ILogger logger, Exception exception, Func<string> messageCreator)
        {
            if (logger != null)
            {
                logger.LogException(LogLevel.Warn, exception, messageCreator);
            }
        }

        /// <summary>
        /// Check if Warn is enabled for this logger
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <returns>True if the level is enabled, false otherwise</returns>
        public static bool IsWarnEnabled(this ILogger logger)
        {
            return logger != null && logger.IsEnabled(LogLevel.Warn);
        }

        #endregion

        #region Error (4)

        /// <summary>
        /// Log a Error message by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        public static void Error(this ILogger logger, string format, params object[] args)
        {
            if (logger != null)
            {
                logger.Log(LogLevel.Error, format, args);
            }
        }

        /// <summary>
        /// Log a Error message by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        public static void Error(this ILogger logger, Func<string> messageCreator)
        {
            if (logger != null)
            {
                logger.Log(LogLevel.Error, messageCreator);
            }
        }

        /// <summary>
        /// Log a Error <see cref="System.Exception"/> by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="exception">Exception</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        public static void ErrorException(this ILogger logger, Exception exception, string format, params object[] args)
        {
            if (logger != null)
            {
                logger.LogException(LogLevel.Error, exception, format, args);
            }
        }

        /// <summary>
        /// Log a Error <see cref="System.Exception"/> by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="exception">Exception</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        public static void ErrorException(this ILogger logger, Exception exception, Func<string> messageCreator)
        {
            if (logger != null)
            {
                logger.LogException(LogLevel.Error, exception, messageCreator);
            }
        }

        /// <summary>
        /// Check if Error is enabled for this logger
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <returns>True if the level is enabled, false otherwise</returns>
        public static bool IsErrorEnabled(this ILogger logger)
        {
            return logger != null && logger.IsEnabled(LogLevel.Error);
        }

        #endregion

        #region Fatal (5)

        /// <summary>
        /// Log a Fatal message by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        public static void Fatal(this ILogger logger, string format, params object[] args)
        {
            if (logger != null)
            {
                logger.Log(LogLevel.Fatal, format, args);
            }
        }

        /// <summary>
        /// Log a Fatal message by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        public static void Fatal(this ILogger logger, Func<string> messageCreator)
        {
            if (logger != null)
            {
                logger.Log(LogLevel.Fatal, messageCreator);
            }
        }

        /// <summary>
        /// Log a Fatal <see cref="System.Exception"/> by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="exception">Exception</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        public static void FatalException(this ILogger logger, Exception exception, string format, params object[] args)
        {
            if (logger != null)
            {
                logger.LogException(LogLevel.Fatal, exception, format, args);
            }
        }

        /// <summary>
        /// Log a Fatal <see cref="System.Exception"/> by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="exception">Exception</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        public static void FatalException(this ILogger logger, Exception exception, Func<string> messageCreator)
        {
            if (logger != null)
            {
                logger.LogException(LogLevel.Fatal, exception, messageCreator);
            }
        }

        /// <summary>
        /// Check if Fatal is enabled for this logger
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <returns>True if the level is enabled, false otherwise</returns>
        public static bool IsFatalEnabled(this ILogger logger)
        {
            return logger != null && logger.IsEnabled(LogLevel.Fatal);
        }

        #endregion

    }
}
