﻿using System;

namespace Acrotech.PortableLogAdapter
{
    /// <summary>
    /// Interface for a custom logger
    /// </summary>
    public partial interface ILogger
    {
        /// <summary>
        /// Logger Name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Log a message at <paramref name="level"/> by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="level">Log Level</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        void Log(LogLevel level, string format, params object[] args);

        /// <summary>
        /// Log a message at <paramref name="level"/> by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="level">Log Level</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        void Log(LogLevel level, Func<string> messageCreator);

        /// <summary>
        /// Log an <see cref="System.Exception"/> at <paramref name="level"/> by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="level">Log Level</param>
        /// <param name="exception">Exception</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        void LogException(LogLevel level, Exception exception, string format, params object[] args);

        /// <summary>
        /// Log an <see cref="System.Exception"/> at <paramref name="level"/> by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="level">Log Level</param>
        /// <param name="exception">Exception</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        void LogException(LogLevel level, Exception exception, Func<string> messageCreator);

        /// <summary>
        /// returns the current log level
        /// </summary>
        LogLevel LogLevel { get; set; }
    }
}
