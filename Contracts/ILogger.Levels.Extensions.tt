﻿<#@ template debug="false" hostspecific="false" language="C#" #>
<#@ output extension=".cs" #>
<#@ include file="../T4/LogLevel.ttinclude" #>
using System;

namespace Acrotech.PortableLogAdapter
{
    /// <summary>
    /// Extensions to simplify logging calls by replacing the level param with an explicit function call
    /// </summary>
    public static partial class ExtensionMethods
    {
<#
	foreach (LogLevel level in Enum.GetValues(typeof(LogLevel)))
    {
		if (level != LogLevel.Off)
		{
			CreateExtensionsForLevel(level);
		}
    }
#>
    }
}
<#+

public void CreateExtensionsForLevel(LogLevel level)
{
#>
        #region <#= level #> (<#= (int)level #>)

        /// <summary>
        /// Log a <#= level #> message by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        public static void <#= level #>(this ILogger logger, string format, params object[] args)
        {
            if (logger != null)
            {
                logger.Log(LogLevel.<#= level #>, format, args);
            }
        }

        /// <summary>
        /// Log a <#= level #> message by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        public static void <#= level #>(this ILogger logger, Func<string> messageCreator)
        {
            if (logger != null)
            {
                logger.Log(LogLevel.<#= level #>, messageCreator);
            }
        }

        /// <summary>
        /// Log a <#= level #> <see cref="System.Exception"/> by formatting the format parameter with the args parameter
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="exception">Exception</param>
        /// <param name="format">Message Format</param>
        /// <param name="args">Message Format Args</param>
        public static void <#= level #>Exception(this ILogger logger, Exception exception, string format, params object[] args)
        {
            if (logger != null)
            {
                logger.LogException(LogLevel.<#= level #>, exception, format, args);
            }
        }

        /// <summary>
        /// Log a <#= level #> <see cref="System.Exception"/> by executing the <paramref name="messageCreator"/> delegate only if the message will be logged
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <param name="exception">Exception</param>
        /// <param name="messageCreator">Message Creator Delegate</param>
        public static void <#= level #>Exception(this ILogger logger, Exception exception, Func<string> messageCreator)
        {
            if (logger != null)
            {
                logger.LogException(LogLevel.<#= level #>, exception, messageCreator);
            }
        }

        /// <summary>
        /// Check if <#= level #> is enabled for this logger
        /// </summary>
        /// <param name="logger">Source Logger</param>
        /// <returns>True if the level is enabled, false otherwise</returns>
        public static bool Is<#= level #>Enabled(this ILogger logger)
        {
            return logger != null && logger.IsEnabled(LogLevel.<#= level #>);
        }

        #endregion

<#+
}

#>