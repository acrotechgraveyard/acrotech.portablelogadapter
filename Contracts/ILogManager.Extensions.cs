﻿using System;
using System.Collections.Generic;

namespace Acrotech.PortableLogAdapter
{
    /// <summary>
    /// Extensions to simplify logger creation
    /// </summary>
    public static partial class ExtensionMethods
    {
        /// <summary>
        /// Simplify translation of a <see cref="System.Type"/> to a logger name
        /// </summary>
        /// <param name="source">Source Type</param>
        /// <returns>Name of the logger based on the provided <paramref name="source"/></returns>
        public static string GetLoggerName(this Type source)
        {
            return source == null ? string.Empty : source.Name;
        }

        /// <summary>
        /// Create a logger without a name (uses an empty string instead)
        /// </summary>
        /// <param name="source">Source Log Manager</param>
        /// <returns>A logger with an empty name</returns>
        public static ILogger GetLogger(this ILogManager source)
        {
            return source == null ? null : source.GetLogger(string.Empty);
        }

        /// <summary>
        /// Create a logger for a object instance (uses the object instance type for name translation)
        /// </summary>
        /// <param name="source">Source Log Manager</param>
        /// <param name="target">Object instance</param>
        /// <returns>Logger with <paramref name="target"/> used for name translation</returns>
        public static ILogger GetLogger(this ILogManager source, object target)
        {
            return source == null ? null : source.GetLogger(target.GetType().GetLoggerName());
        }

        /// <summary>
        /// Create a logger using a specified type for name translation
        /// </summary>
        /// <param name="source">Source Log Manager</param>
        /// <param name="type">Type for name translation</param>
        /// <returns>Logger with <paramref name="type"/> used for name translation</returns>
        public static ILogger GetLogger(this ILogManager source, Type type)
        {
            return source == null ? null : source.GetLogger(type.GetLoggerName());
        }

        /// <summary>
        /// Create a logger using a specified generic type for name translation
        /// </summary>
        /// <typeparam name="T">Type for name translation</typeparam>
        /// <param name="source">Source Log Manager</param>
        /// <returns>Logger with <typeparamref name="T"/> used for name translation</returns>
        public static ILogger GetLogger<T>(this ILogManager source)
        {
            return source == null ? null : source.GetLogger(typeof(T).GetLoggerName());
        }

        /// <summary>
        /// Returns all valid log levels that can be used to log a message
        /// </summary>
        /// <param name="source">Log manager</param>
        /// <returns>All valid log levels</returns>
        /// <remarks>Returns all LogLevel enum values except Off</remarks>
        public static IEnumerable<LogLevel> GetAllLogLevels(this ILogManager source)
        {
            return ExtensionMethods.GetAllLogLevels();
        }
    }
}
