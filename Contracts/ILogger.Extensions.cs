﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acrotech.PortableLogAdapter
{
    public static partial class ExtensionMethods
    {
        /// <summary>
        /// Check if <paramref name="level"/> is enabled for <paramref name="source"/>
        /// </summary>
        /// <param name="source">Logger to check against</param>
        /// <param name="level">Log Level</param>
        /// <returns>True if the level is enabled, false otherwise</returns>
        public static bool IsEnabled(this ILogger source, LogLevel level)
        {
            return level.IsEnabledFor(source.LogLevel);
        }

        /// <summary>
        /// Returns all valid log levels that can be used to log a message
        /// </summary>
        /// <param name="source">Logger</param>
        /// <returns>All valid log levels</returns>
        /// <remarks>Returns all LogLevel enum values except Off</remarks>
        public static IEnumerable<LogLevel> GetAllLogLevels(this ILogger source)
        {
            return ExtensionMethods.GetAllLogLevels();
        }
    }
}
