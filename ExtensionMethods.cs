﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acrotech.PortableLogAdapter
{
    public static partial class ExtensionMethods
    {
        /// <summary>
        /// Format the <paramref name="format"/> with the provided <paramref name="args"/> only if the format is non-null and non-empty. 
        /// This call will catch all exceptions and use the <paramref name="format"/> for return value.
        /// </summary>
        /// <param name="format">String to Format</param>
        /// <param name="args">Format Args</param>
        /// <returns>The formatted string, or <paramref name="format"/> on any error</returns>
        public static string FormatSafe(this string format, params object[] args)
        {
            var message = format;

            if (string.IsNullOrEmpty(format) == false && args != null && args.Length > 0)
            {
                try
                {
                    message = string.Format(format, args);
                }
                catch (FormatException)
                {
                }
            }

            return message;
        }
    }
}
