﻿# README #

This library is intended to be a drop in replacement for an abstracted logging interface. This library was designed to specifically work very well with `NLog` but can easily be adapted to use any logging system. By using this library, you can easily use dependency injection or simple construction to produce any loggers required. The interface is fully scalable to any type of logging system and can be adjusted very easily using the T4 templates to produce new extension methods or logging levels. This library is built as a PCL to support most common deployments without any additional immediate dependencies (platform dependencies required for the specific logging system desired, or none if you're happy with `Debug.WriteLine`.

### What is this repository for? ###

* Anyone who is writing a cross platform application and wants to include cross platform logging with minimal non-shared code.

### How do I get set up? ###

0. Include this library
0. Implement the custom ILogManager and ILogger (see unit tests for examples)
0. Use dependency injection (with ILogManager) or create Singleton accessors to acquire ILogger instances

### Who do I talk to? ###

* Contact me for suggestions, improvements, or bugs

### Changelog ###

#### 1.0.1.0 ####

* Improving NuGet build process by using the developmentDependency attribute
* Minor NuGet metadata adjustments
* Updating FormatSafe to break out early from format attempts when there are no args to format with
* Removing build dependency on the Tangible T4 editor
* Cleaning up T4 template generated files by using spaces instead of tabs for verbatim text
* Updating PCL profile to 344 (maximum compatibility)

#### 1.0.0.7 ####

* Fixing namespace for VersionDetails (forgot to update it after updating the NuGet package)

#### 1.0.0.6 ####

* Updating Versioning to fix an issue with a global class name collision

#### 1.0.0.5 ####

* Fixing some NuGet build issues

#### 1.0.0.4 ####

* Using Acrotech.Versioning for versioning

#### 1.0.0.3 ####

* Adding XMLDOC comments (and including the XML file in the NuGet package)
* Updating some NuGet related files

#### 1.0.0.2 ####

* Eliminating the .Contracts namespace to simplify access to the interfaces and extension methods (sorry for breaking changes)
* Eliminating the .Types namespace to simplify access to the interfaces and extension methods (sorry for breaking changes)
* Code cleanup

#### 1.0.0.1 ####

* Improved unit tests
* A few more extension methods to simplify log creation

#### 1.0.0.0 ####

* Initial Release
