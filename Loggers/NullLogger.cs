﻿using System;

namespace Acrotech.PortableLogAdapter.Loggers
{
    /// <summary>
    /// Null Logger simply ignores all log calls.
    /// </summary>
    public class NullLogger : ILogger
    {
        /// <summary>
        /// Null Logger Name
        /// </summary>
        public const string LoggerName = "Null Logger";

        /// <summary>
        /// Default (and only available) Null Logger Instance
        /// </summary>
        public static readonly NullLogger Default = new NullLogger();

        private NullLogger()
        {
        }

        /// <inheritdoc/>
        public string Name { get { return LoggerName; } }

        /// <inheritdoc/>
        /// <remarks>This will not log anything</remarks>
        public void Log(LogLevel level, string format, params object[] args)
        {
            // do nothing
        }

        /// <inheritdoc/>
        /// <remarks>This will not log anything</remarks>
        public void Log(LogLevel level, Func<string> messageCreator)
        {
            // do nothing
        }

        /// <inheritdoc/>
        /// <remarks>This will not log anything</remarks>
        public void LogException(LogLevel level, Exception exception, string format, params object[] args)
        {
            // do nothing
        }

        /// <inheritdoc/>
        /// <remarks>This will not log anything</remarks>
        public void LogException(LogLevel level, Exception exception, Func<string> messageCreator)
        {
            // do nothing
        }

        /// <inheritdoc/>
        /// <remarks>Always returns Off</remarks>
        public LogLevel LogLevel { get { return LogLevel.Off; } set { throw new InvalidOperationException("Null Logger Cannot Change Log Level"); } }
    }
}
