﻿using System;

namespace Acrotech.PortableLogAdapter.Loggers
{
    /// <summary>
    /// Simple logger that forwards all log calls to the provided delegate action
    /// </summary>
    public class DelegateLogger : ILogger
    {
        /// <summary>
        /// Creates a simple name and delegate based delegate logger
        /// </summary>
        /// <param name="name">Logger name</param>
        /// <param name="level">Log level</param>
        /// <param name="delegateAction">Logger delegate action</param>
        public DelegateLogger(string name, LogLevel level, Action<string> delegateAction)
        {
            Name = name;
            DelegateAction = delegateAction;
        }

        /// <inheritdoc/>
        public string Name { get; private set; }

        /// <inheritdoc/>
        public Action<string> DelegateAction { get; private set; }

        /// <inheritdoc/>
        public void Log(LogLevel level, string format, params object[] args)
        {
            if (DelegateAction != null)
            {
                // -5 ensures that all levels consume the same number of characters (i.e. right padding with spaces)
                DelegateAction(string.Format("[{0,-5}] {1}", level, format.FormatSafe(args)));
            }
        }

        /// <inheritdoc/>
        public void Log(LogLevel level, Func<string> messageCreator)
        {
            Log(level, messageCreator());
        }

        /// <inheritdoc/>
        public void LogException(LogLevel level, Exception exception, string format, params object[] args)
        {
            Log(level, string.Format(format, args) + ": {0}", exception);
        }

        /// <inheritdoc/>
        public void LogException(LogLevel level, Exception exception, Func<string> messageCreator)
        {
            LogException(level, exception, messageCreator());
        }

        /// <inheritdoc/>
        public LogLevel LogLevel { get; set; }
    }
}
