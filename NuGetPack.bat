SET MSBUILD="C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe"
SET TT="%CommonProgramFiles(x86)%\microsoft shared\TextTemplating\12.0\TextTransform.exe"

%TT% "Properties\VersionDetails.tt" -a !!ManualBuild!True

%MSBUILD% Acrotech.PortableLogAdapter.csproj /t:Rebuild /p:Configuration=Release /p:Platform=AnyCPU /p:VisualStudioVersion=12.0

DEL *.nupkg

..\.nuget\NuGet.exe Pack -Prop Configuration=Release

PAUSE
